# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言
import os
import sys
import pytesseract
from PIL import Image
import logging
from datetime import datetime



def ocr_imgcode(element):

    current_path = sys.path[0]
    logging.info('当前目录 ==%s'%current_path)
    img_full_path = '%s%s' % (current_path, '/img/imgcode_%s.png'% datetime.timestamp(datetime.now()))
    img_full_path = img_full_path.replace("\\", "/")

    img_path = '%s/img'%current_path
    img_path = img_path.replace("\\", "/")

    # 自动创建img目录
    if os.path.exists(img_path):
        if os.path.isdir(img_path):
            logging.info('img目录已存在：%s'% img_path)
        elif os.path.isfile(img_path):
            os.mkdir(img_path)
            logging.info('创建img目录：%s'% img_path)
    else:
        os.mkdir(img_path)
        logging.info('创建img目录：%s' % img_path)

    element.screenshot(img_full_path)  # 截图

    # 读取png文件，进行ocr识别
    image = Image.open(img_full_path)

    # 判断操作系统，调用不同的OCR语言包
    logging.info(sys.platform.lower())
    if 'win' in sys.platform.lower():
        text = pytesseract.image_to_string(image)
    elif sys.platform.lower() == 'linux':
        text = pytesseract.image_to_string(image, 'afr')  # , lang='art' ,Linux 系统需要加参数
    else:
        logging.error('未知平台，无法进行OCR识别：%s' % sys.platform.lower())
        return None

    text = text.replace('$', '8')
    text = text.replace('A', '4')
    text = text.replace('O', '0')
    text = text.replace('.', '')
    logging.info('【%s】识别后为：%s' % (img_full_path, text))
    text = text.strip()

    # 删除文件
    if os.path.exists(img_full_path):
        os.remove(img_full_path)

    if text != None and len(text) == 4 and text.isdigit():
        return text
    else:
        return None