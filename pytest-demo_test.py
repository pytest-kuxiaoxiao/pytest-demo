# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言


import sys
import time
import pytesseract
from PIL import Image
import requests
import allure
import pytest
import logging
from tools import tools

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import Chrome

@allure.feature('功能模块')  #模块
class Test_1:
    url = None
    driver = None


    @classmethod  #使用pytest 命令时，执行用例前的初始化方法，自动调用
    def setup_class(self):
        self.url = "https://cc.10010.com/cs/ccc/online/web/hollysns/web/page/robot.html?channelId=243aefb78c2c4c94ab26f3ce7c958e0c"
        # co = Options()
        # co.add_experimental_option('excludeSwitches', ['enable-automation'])
        # co.add_experimental_option('useAutomationExtension', False)  # =>去掉浏览器正在受自动测试软件的控制
        # co.add_experimental_option('detach', True)  # 运行完不自动关闭浏览器


        co = Options()
        co.add_argument('--no-sandbox')  #
        co.add_experimental_option('excludeSwitches', ['enable-automation'])
        co.add_experimental_option('useAutomationExtension', False)  # =>去掉浏览器正在受自动测试软件的控制
        co.add_experimental_option('detach', True)  # 运行完不自动关闭浏览器
        co.add_argument('--headless')  # =>Chrome配置默认为无头模式

        try:
            self.driver = webdriver.Chrome(executable_path="chromedriver.exe",options=co)
            # self.driver = webdriver.Chrome(executable_path="chromedriver", options=co)
        except:
            self.driver = webdriver.Chrome(executable_path="chromedriver", options=co)

        print("初始化")
        logging.info("初始化方法")

    @classmethod  # 使用pytest 命令时，自动调用
    def teardown_class(self):
        print("运行结束end")
        # logging.info("结束方法")


    @pytest.mark.run(order=8)
    @allure.story('子功能1')      #子模块
    def test_1(self):               #用例

        self.driver.get(self.url)
        time.sleep(4)

        # list1 = self.driver.find_elements_by_tag_name('iframe')
        # logging.error(len(list1))
        #
        # for e in list1:
        #     logging.error(type(e))
        #     logging.error(e.get_property('id'))

        # self.driver.switch_to_frame('ueditor_0')
        self.driver.switch_to.frame(0)

        self.driver.find_element_by_xpath('//*[@contenteditable="true"]/p').send_keys('转人工')
        # ueditor = self.driver.find_element_by_xpath('//*[@contenteditable="true"]/p')
        time.sleep(3)
        # self.driver.find_element_by_xpath('//*[@contenteditable="true"]/p').send_keys()
        self.driver.switch_to.default_content()
        self.driver.find_element_by_xpath("//button[text()='发送']").click()

        time.sleep(5)
        # 点击 去验证
        self.driver.find_element_by_xpath("//*[text()='去验证']").click()

        time.sleep(5)
        # 点击 服务密码
        self.driver.find_element_by_xpath("//*[text()='服务密码' and @role = 'tab']").click()

        time.sleep(5)

        # img_path = sys.path[0]
        # logging.error(img_path)
        # img_full_path = '%s%s' %(img_path,'/img/imgcode.png')
        # img_full_path = img_full_path.replace("\\", "/")
        #
        # logging.error(img_full_path)
        # self.driver.find_element_by_class_name('verification').find_element_by_tag_name('img').screenshot(img_full_path)
        #
        # image = Image.open(img_full_path)
        # logging.info(image)
        # text = pytesseract.image_to_string(image)  # , lang='chi_sim'
        # text = text.replace('$','8')
        #
        # logging.info(text)
        # logging.error('text==%s',text)

        text = tools.ocr_imgcode(self.driver.find_element_by_class_name('verification').find_element_by_tag_name('img'))

        logging.info('text=%s'%text)

        self.driver.close()




        assert True
    #
    # @pytest.mark.run(order=5)
    # @allure.story('子功能2')  # 子模块
    # def test_2(self):               #用例
    #     logging.info("test2222222222")
    #     print('start2222222222222222222222222222222222.')
    #     # res=requests.get("http://172.17.200.218:8080/findAll?id=1")
    #     # print(res.text)
    #     assert 1==1


if __name__ == '__main__':
    pytest.main(['-s', '-q', '--alluredir', './report/xml'])


#Test_1.test_1(Test_1)